<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Dashboard Page

Route::get('/', 'DashboardController@index')->name('dashboard');
Route::get('/cari', 'DashboardController@fetch');

// Ebook Page

Route::get('/ebook', 'EbookController@index')->name('ebook');
Route::get('/ebook/fetchdata/{id}', 'EbookController@show');
Route::get('/ebook/pdfpreview/{id}', 'EbookController@pdf');
Route::get('/ebook/result', 'EbookController@cari');
Route::get('/ebook/detailebook/{id}/{kategori}', 'EbookController@detail');
Route::post('/ebook/tambahebook', 'EbookController@store')->name('tambahebook');
Route::post('/ebook/ubahdata/{id}', 'EbookController@update');
Route::post('/ebook/hapusdata/{id}', 'EbookController@destroy');

// Video Page

Route::get('/video', 'VideoController@index')->name('video');
Route::get('/video/fetchdata/{id}', 'EbookController@show');
Route::get('/video/result', 'VideoController@cari');
Route::get('/video/detailvideo/{id}/{kategori}', 'VideoController@detail');
Route::post('/video/tambahvideo', 'VideoController@store')->name('tambahvideo');
Route::post('/video/ubahdata/{id}', 'VideoController@update');
Route::post('/video/hapusdata/{id}', 'VideoController@destroy');

// Shop Page

Route::get('/shop', 'ShopController@index')->name('shop');
Route::get('/shop/fetchdata/{id}', 'EbookController@show');
Route::get('/shop/result', 'ShopController@cari');
Route::get('/shop/detailproduk/{id}/{kategori}', 'ShopController@detail');
Route::post('/shop/tambahproduk', 'ShopController@store')->name('tambahshop');
Route::post('/shop/ubahdata/{id}', 'ShopController@update');
Route::post('/shop/hapusdata/{id}', 'ShopController@destroy');

// Promo Page

Route::get('/promo', 'PromoController@index')->name('promo');
Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');
