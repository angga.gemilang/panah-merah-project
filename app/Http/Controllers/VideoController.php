<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Data;
use App\TipeData;
use Session;
use Image;
use File;

class VideoController extends Controller
{
    public function index()
    {
        $tipe = TipeData::all();
        $video = Data::where('id_tipe_data', '1')->get();
        return view ('video', ['tipe' => $tipe,
                               'video' => $video ]);
    }

    public function detail($id)
    {
        $related = Data::where('id_tipe_data','1')
                                ->whereNotIn('id',[$id])               
                                ->get();
        $dvideo = Data::find($id);
        return view ('detailvideo', ['dvideo' => $dvideo,
                                     'related' => $related]);
    }

    public function cari(Request $request){

		$keyword = $request->get('search_query');
        $tipe = TipeData::all();
        $video = Data::where('id_tipe_data','1')
                                ->where('judul','like','%' . $keyword . '%')
		                        ->get();
        return view ('video', ['tipe' => $tipe,
                               'video' => $video ]);
	}

    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|max:1024',
            'deskripsi' => 'required|max:1024',
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'thumbnail' => 'image|mimes:jpeg,png,jpg,gif,svg|max:3024',
            'kategori' => 'required|max:1024',
            'id_tipe_data' => 'required|max:1024',
        ]);

        $thumbnail = $request->file('thumbnail');
		$namathumbnail = 'thumbnailvideo' . Carbon::now()->format('YmdHis') . '.' . $thumbnail->getClientOriginalExtension(); 
        Image::make(File::get($thumbnail))->resize('420','236')->save('uploaded/pic/'. $namathumbnail);

        $t_video = new Data;
        $t_video->judul = $request->judul;
        $t_video->deskripsi = $request->deskripsi;
        $t_video->kategori = $request->kategori;
        $t_video->file = $request->link;
        $t_video->thumbnail = $namathumbnail;
        $t_video->id_tipe_data = $request->id_tipe_data;
        $t_video->save();
        
		Session::flash('success','Video Berhasil Ditambahkan');

        return redirect()->back();
    }

    public function show($id)
    {
        return Data::find($id);
    }

    public function update(Request $request, $id)
    {
        $e_video = Data::find($id);
        $e_video->judul = $request->judul;
        $e_video->deskripsi = $request->deskripsi;
        $e_video->kategori = $request->kategori;
        $e_video->file = $request->link;
        $e_video->id_tipe_data = $request->id_tipe_data;

        if($request->hasfile('thumbnail')){
            $imagethumbnail = public_path("uploaded/pic/" . $e_video->thumbnail);
            \File::delete($imagethumbnail);
            $thumbnail = $request->file('thumbnail');
            $namathumbnail = 'thumbnailvideoupdate' . Carbon::now()->format('YmdHis') . '.' . $thumbnail->getClientOriginalExtension(); 
            Image::make(File::get($thumbnail))->resize('420','236')->save('uploaded/pic/'. $namathumbnail);
            $e_video->thumbnail = $namathumbnail;
        }

        $e_video->update();

        Session::flash('success','Video Berhasil Diperbaharui');

        return redirect()->back();
    }

    public function destroy($id)
    {
        $d_video = Data::find($id);
        $imagethumbnail = public_path("uploaded/pic/" . $d_video->thumbnail);
        if(\File::exists($imagethumbnail)){
            \File::delete($imagethumbnail);
        }
        $d_video->delete();

		Session::flash('success','Video Berhasil Dihapus');

        return redirect()->back();
    }
}
