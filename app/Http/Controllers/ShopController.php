<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Imports\DataImport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Data;
use App\TipeData;
use Session;
use Image;
use File;

class ShopController extends Controller
{

    public function index()
    {
        $tipe = TipeData::all();
        $produk = Data::where('id_tipe_data','3')->get();
        return view ('shop', ['tipe' => $tipe,
                              'produk' => $produk ]);
    }

    public function detail($id, $kategori)
    {
        $dshop = Data::find($id);
        $relate = Data::where('kategori', $kategori)
                                ->whereNotIn('id',[$id])
                                ->where('id_tipe_data','=','3')
                                ->orderBy('id','desc')
                                ->get();
        $related = $relate->shuffle()->slice(0,6);
        return view ('detailshop', ['dshop' => $dshop,
                                    'related' => $related ]);
    }

    public function cari(Request $request){

		$keyword = $request->get('search_query');
        $tipe = TipeData::all();
        $produk = Data::where('id_tipe_data','3')
                                ->where('judul','like','%' . $keyword . '%')
		                        ->get();
        return view ('shop', ['tipe' => $tipe,
                               'produk' => $produk ]);
	}

    public function store(Request $request)
    {
        Excel::import(new DataImport,request()->file('filedata'));
		Session::flash('success','Produk Berhasil Ditambahkan');
        return redirect()->back();
    }

    public function show($id)
    {
        return Data::find($id);
    }

    public function update(Request $request, $id)
    {
        $e_produk = Data::find($id);
        $e_produk->judul = $request->judul;
        $e_produk->deskripsi = $request->deskripsi;
        $e_produk->kategori = $request->kategori;
        
        if($request->hasfile('thumbnail')){
            $imagethumbnail = public_path("uploaded/pic/" . $e_produk->thumbnail);
            \File::delete($imagethumbnail);
            $thumbnail = $request->file('thumbnail');
            $namathumbnail = 'thumbnailprodukupdate' . Carbon::now()->format('YmdHis') . '.' . $thumbnail->getClientOriginalExtension(); 
            Image::make(File::get($thumbnail))->resize(280, 285)->save('uploaded/pic/'. $namathumbnail);
            $e_produk->thumbnail = $namathumbnail;
        }

        $e_produk->id_tipe_data = $request->id_tipe_data;
        $e_produk->link_bl = $request->link_bl;
        $e_produk->link_tp = $request->link_tp;
        $e_produk->link_sp = $request->link_sp;
        $e_produk->update();

        Session::flash('success','Produk Berhasil Dihapus');
        
        return redirect()->back();
    }

    public function destroy($id)
    {
        $d_produk = Data::find($id);
        $imagethumbnail = public_path("uploaded/pic/" . $d_produk->thumbnail);
        if(\File::exists($imagethumbnail)){
            \File::delete($imagethumbnail);
        }
        $d_produk->delete();

		Session::flash('success','Produk Berhasil Dihapus');

        return redirect()->back();
    }
}
