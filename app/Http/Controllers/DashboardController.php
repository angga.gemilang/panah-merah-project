<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Data;

class DashboardController extends Controller
{
    public function index(){
        $ebook = Data::where('id_tipe_data','2')->get();
        $slice = $ebook->shuffle()->slice(0,7);

        $video = Data::where('id_tipe_data','1')->get();
        $slicevid = $video->shuffle()->slice(0,3);

        $produk = Data::where('id_tipe_data','3')->get();
        $sliceprod = $produk->shuffle()->slice(0,7);
        return view ('dashboard', ['slice' => $slice,
                                   'slicevid' => $slicevid,
                                   'sliceprod' => $sliceprod ]);
    }

    public function fetch(Request $request)
    {
        if($request->get('query'))
        {
            $query = $request->get('query');
            $data = Data::where('judul', 'LIKE', "%{$query}%")->get();
            foreach($data as $kategori)
            {
            $output = '<ul class="dropdown-menu" style="border-radius: 5px; overflow: auto; max-height: 200px; padding: 0; margin: 0; width: 474px; display:block; position: relative;">';
            if ($kategori->id_tipe_data == "1"){$tipedatakategori = "Video";} else if ($kategori->id_tipe_data == "2"){ $tipedatakategori = "Ebook"; } else{$tipedatakategori = "Produk";}
                $output .= '<h5 class="pl-3 pt-2 text-danger">' . $tipedatakategori . '</h5>';
                foreach($data as $row)
                {
                    if($kategori->id_tipe_data === $row->id_tipe_data)
                    {
                        if ($row->id_tipe_data == "1"){$tipedata = "Video";} else if ($row->id_tipe_data == "2"){ $tipedata = "Ebook"; } else{$tipedata = "Produk";}
                        if ($tipedata == "Ebook"){$linkawal = "/ebook/detailebook/";}else if($tipedata == "Video"){$linkawal = "/video/detailvideo/";}else{$linkawal = "/shop/detailproduk/";} 
                        $output .= '<a id="link-output" href="' . $linkawal . $row->id . '/' . $row->kategori . '"><li id="item-li"><img style="width: 60px; padding-right: 15px;" src="/uploaded/pic/' . $row->thumbnail . '">' . $row->judul . '</li></a>';
                    }
                }
                $output .= '</ul>';
            }
            echo $output;
        }
    }
}