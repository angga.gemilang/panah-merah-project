<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Data;
use App\TipeData;
use Session;
use Image;
use File;

class EbookController extends Controller
{

    public function index()
    {
        $tipe = TipeData::all();
        $ebook = Data::where('id_tipe_data','2')->get();
        return view ('ebook', ['tipe' => $tipe,
                                'ebook' => $ebook ]);
    }

	public function cari(Request $request){

		$keyword = $request->get('search_query');
        $tipe = TipeData::all();
        $ebook = Data::where('id_tipe_data','2')
                                ->where('judul','like','%' . $keyword . '%')
		                        ->get();
        return view ('ebook', ['tipe' => $tipe,
                               'ebook' => $ebook ]);
	}

    public function detail($id,$kategori)
    {
        $debook = Data::find($id);
        $relate = Data::where('kategori', $kategori)
                                    ->where('id_tipe_data','2')
                                    ->get();
        $related = $relate->shuffle()->slice(0,15);
        return view ('detailebook', ['debook' => $debook,
                                     'related' => $related ]);
    }

    public function store(Request $request)
    {        
        $this->validate($request, [
            'judul' => 'required|max:1024',
            'deskripsi' => 'required|max:1024',
            'kategori' => 'required|max:1024',
            'id_tipe_data' => 'required|max:1024',
        ]);

		$file = $request->file('file');
        $namafile = 'Ayo Menanam PDF ' . Carbon::now()->format('Ymd') . ' at ' . Carbon::now()->format('His') .  '.' . $file->getClientOriginalExtension(); 
        $targetdir = 'uploaded/file';
		$file->move($targetdir, $namafile);
            
        $thumbnail = $request->file('thumbnail');
        $namathumbnail = 'Ayo Menanam Thumbnail ' . Carbon::now()->format('Ymd') . ' at ' . Carbon::now()->format('His') .  '.' . $thumbnail->getClientOriginalExtension(); 
        Image::make(File::get($thumbnail))->resize(180, 185)->save('uploaded/pic/'. $namathumbnail);

        $t_ebook = new Data;
        $t_ebook->judul = $request->judul;
        $t_ebook->deskripsi = $request->deskripsi;
        $t_ebook->kategori = $request->kategori;
        $t_ebook->id_tipe_data = $request->id_tipe_data;
        $t_ebook->file = $namafile;
        $t_ebook->thumbnail = $namathumbnail;
        $t_ebook->save();

		Session::flash('success','Ebook Berhasil Ditambahkan');

        return redirect()->back();
    }

    public function show($id)
    {
        return Data::find($id);
    }

    public function pdf($id)
    {
        $pdf = Data::find($id);
        return view ('detailpdf', ['pdf' => $pdf]);
    }

    public function update(Request $request, $id)
    {
        $e_ebook = Data::find($id);
        $e_ebook->judul = $request->judul;
        $e_ebook->deskripsi = $request->deskripsi;
        $e_ebook->kategori = $request->kategori;
        $e_ebook->id_tipe_data = $request->id_tipe_data;

        if($request->hasfile('thumbnail')){
            $imagethumbnail = public_path("uploaded/pic/" . $e_ebook->thumbnail);
            \File::delete($imagethumbnail);
            $thumbnail = $request->file('thumbnail');
            $namathumbnail = 'Ayo Menanam Thumbnail ' . Carbon::now()->format('Y-m-d') . ' at ' . Carbon::now()->format('H:i:s') .  '.' . $thumbnail->getClientOriginalExtension();             Image::make(File::get($thumbnail))->resize(180, 185)->save('uploaded/pic/'. $namathumbnail);
            $e_ebook->thumbnail = $namathumbnail;
        }

        if($request->hasfile('file')){
            $filepdf = public_path("uploaded/file/" . $e_ebook->file);
            \File::delete($filepdf);
            $file = $request->file('file');
            $namafile = 'Ayo Menanam PDF ' . Carbon::now()->format('Y-m-d') . ' at ' . Carbon::now()->format('H:i:s') .  '.' . $file->getClientOriginalExtension(); 
            $targetdir = 'uploaded/file';
            $file->move($targetdir, $namafile);
            $e_ebook->file = $namafile;
        }
        $e_ebook->update();

		Session::flash('success','Ebook Berhasil Diperbaharui');        

        return redirect()->back();

    }

    public function destroy($id)
    {
        $d_ebook = Data::find($id);
        $imagethumbnail = public_path("uploaded/pic/" . $d_ebook->thumbnail);
        $filepdf = public_path("uploaded/file/" . $d_ebook->file);
        if(\File::exists($imagethumbnail)){
            \File::delete($imagethumbnail);
        }
        if(\File::exists($filepdf)){
            \File::delete($filepdf);
        }
        $d_ebook->delete();

		Session::flash('success','Ebook Berhasil Dihapus');

        return redirect()->back();
    }

}