<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipeData extends Model
{
	protected $table = "tipe_data";

	protected $fillable = [
        'tipe'
    ];
}
