<?php

namespace App\Imports;

use App\Data;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DataImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Data([
            'judul' => $row['judul'],
            'deskripsi' => $row['deskripsi'],
            'kategori' => $row['kategori'],
            'thumbnail' => $row['thumbnail'],
            'link_tp' => $row['link_tokopedia'],
            'link_bl' => $row['link_bukalapak'],
            'link_sp' => $row['link_shopee'],
            'id_tipe_data' => 3,
        ]);
    }
}
