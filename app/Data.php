<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $table = 'data';

    protected $fillable  = [
        'judul','deskripsi','thumbnail','kategori','file','id_tipe_data',
    ];
}