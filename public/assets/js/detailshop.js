$(function () {

    $(".exzoom").exzoom({

        "navWidth": 60,
        "navHeight": 60,
        "navItemNum": 5,
        "navItemMargin": 7,
        "navBorder": 1,

        // autoplay
        "autoPlay": false,

        // autoplay interval in milliseconds
        "autoPlayTimeout": 2000

    });

});

$(document).ready(function () {

    $('#tabs li a:not(:first)').addClass('inactive');
    $('.containe4r').hide();
    $('.containe4r:first').show();

    $('#tabs li a').click(function () {
        var t = $(this).attr('id');
        if ($(this).hasClass('inactive')) { //this is the start of our condition 
            $('#tabs li a').addClass('inactive');
            $(this).removeClass('inactive');

            $('.containe4r').hide();
            $('#' + t + 'C').fadeIn('slow');
        }
    });
});


$(document).ready(function () {

    $('.owl-carousel.shop').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        dots: false,
        navText: ["<i class='fa fa-angle-left text-dark'></i>", "<i class='fa fa-angle-right text-dark'></i>"],
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: true
            },
            1000: {
                items: 5,
                nav: true,
                loop: true
            }
        }
    })

    $('.item-produk').on('mouseover', function () {
        $(this).css({
            transform: 'scale(1.004)',
            cursor: 'pointer',
            boxShadow: '0px 1.2px 4px rgba(0,0,0,0.2)'
        });
        $(this).find('.judul-barang').css('color', '#C00000');
    }).on('mouseout', function () {
        $(this).css({
            transform: 'scale(1)',
            cursor: 'auto',
            boxShadow: 'none'
        })
        $(this).find('.judul-barang').css('color', 'black');
    });

    $('.i-tp').on('mouseover', function () {
        $(this).next().css({
            visibility: 'visible',
            opacity: '1',
            transition: '.5s',
            transitionDelay: '.3s'
        })
    }).on('mouseout', function () {
        $(this).next().css({
            transitionDelay: '.5s',
            transition: '.5s',
            visibility: 'hidden',
            opacity: '0',
        })
    })

    $('.i-sp').on('mouseover', function () {
        $(this).next().css({
            visibility: 'visible',
            opacity: '1',
            transition: '.5s',
            transitionDelay: '.3s'
        })
    }).on('mouseout', function () {
        $(this).next().css({
            transitionDelay: '.5s',
            transition: '.5s',
            visibility: 'hidden',
            opacity: '0'
        })
    })

    $('.i-bl').on('mouseover', function () {
        $(this).next().css({
            visibility: 'visible',
            opacity: '1',
            transition: '.5s',
            transitionDelay: '.3s'
        })
    }).on('mouseout', function () {
        $(this).next().css({
            transitionDelay: '2s',
            transition: '.5s',
            visibility: 'hidden',
            opacity: '0',
            transition: '.5s'
        })
    })
})

$(document).ready(function () {

    $('.full-text').hide();
    $('.view-btn').click(function () {
        $('.full-text').slideToggle('slow');
        $(this).prev().attr('class', $(this).prev().attr('class')==='fas fa-chevron-left'?'fas fa-chevron-right':'fas fa-chevron-left');
        $(this).text($(this).text() === 'Read less' ? 'Read more' : 'Read less');
    });
});