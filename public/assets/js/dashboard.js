// image slider

$(document).ready(function () {

    var timingRun;

    var images = [];
    images[0] = 'https://images.unsplash.com/photo-1519462568576-0c687427fb2e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60';
    images[1] = 'https://images.unsplash.com/photo-1510844355160-2fb07bf9af75?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60';
    images[2] = 'https://images.unsplash.com/photo-1485277068030-a29993c5d5f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60';
    images[3] = 'https://images.unsplash.com/photo-1523349312806-f5dde0a01c32?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60';

    var capTexts = [];
    capTexts[0] = 'Lorem Ipsum';
    capTexts[1] = 'Lorem Ipsum';
    capTexts[2] = 'Lorem Ipsum';
    capTexts[3] = 'Lorem Ipsum';

    var pausebetweenimages = 8000;

    var count = 0;
    var imageCount = images.length - 1;

    var navDots = [];

    for (var i = 0; i < imageCount + 1; i++) {
        navDots[i] = '<div class="dot"></div>';
        $('.dot-container').append(navDots[i]);
    }

    $('.main-content').html('<img src=' + images[count] + '>');

    $('.main-content > img').addClass('responsive-img');

    $('.dot').eq(count).addClass('dotactive');

    $('.captionText').animate({
        'left': '40px',
        'opacity': '.8'
    }, 500);
    $('.captionText').text(capTexts[count]);

    timingRun = setInterval(function () {
        sliderTiming();
    }, pausebetweenimages);

    function sliderTiming() {
        $('.dot').eq(count).removeClass('dotactive');
        count++;

        if (count > imageCount) {
            count = 0;
        }

        //fadein and fadeout effect

        $('.main-content').fadeOut(100, function () {
            $('.main-content').html('<img src=' + images[count] + '>');
            $('.main-content > img').addClass('responsive-img');
            $('.dot').eq(count).addClass('dotactive');
            $('.main-content').fadeIn(500);

            captextAnim_Responsive();
        });
    }


    $('.right-arrow').click(function () {
        $('.dot').eq(count).removeClass('dotactive');
        count++;

        if (count > imageCount) {
            count = 0;
        }
        $('.main-content').fadeOut(100, function () {
            $('.main-content').html('<img src=' + images[count] + '>');
            $('.main-content > img').addClass('responsive-img');
            $('.dot').eq(count).addClass('dotactive');
            $('.main-content').fadeIn(500);

            captextAnim_Responsive();
        });

        resetTiming();

    });

    $('.left-arrow').click(function () {
        $('.dot').eq(count).removeClass('dotactive');
        count--;
        if (count < 0) {
            count = imageCount;
        }
        $('.main-content').fadeOut(100, function () {
            $('.main-content').html('<img src=' + images[count] + '>');
            $('.main-content > img').addClass('responsive-img');
            $('.dot').eq(count).addClass('dotactive');
            $('.main-content').fadeIn(500);

            captextAnim_Responsive();
        });
        resetTiming();
    });

    $('.dot').click(function () {
        $('.dot').eq(count).removeClass('dotactive');
        count = $(this).index();
        $('.main-content').fadeOut(100, function () {
            $('.main-content').html('<img src=' + images[count] + '>');
            $('.main-content > img').addClass('responsive-img');
            $('.dot').eq(count).addClass('dotactive');
            $('.main-content').fadeIn(500);

            captextAnim_Responsive();
        });
        resetTiming();
    });

    //reset the time interval

    function resetTiming() {
        clearInterval(timingRun);
        timingRun = setInterval(function () {
            sliderTiming();
        }, pausebetweenimages);
    }

    //animating caption text with responsiveness

    function captextAnim_Responsive() {
        if ($(window).width() <= 500) {
            $('.captionText').css('opacity', '0');
            $('.captionText').css('left', '0');
            $('.captionText').text(capTexts[count]);
            $('.captionText').animate({
                'left': '10px',
                'opacity': '.8'
            }, 500);
        } else {
            $('.captionText').css('opacity', '0');
            $('.captionText').css('left', '0');
            $('.captionText').text(capTexts[count]);
            $('.captionText').animate({
                'left': '40px',
                'opacity': '.8'
            }, 500);
        }
    }
});

$(document).ready(function () {

    $('.owl-carousel.ebook').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        dots: false,
        navText: ["<i class='fa fa-angle-left text-dark'></i>", "<i class='fa fa-angle-right text-dark'></i>"],
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: true
            },
            1000: {
                items: 5,
                nav: true,
                loop: true
            }
        }
    })

    $('.link-ebook').on('mouseover', function () {
        $(this).css({
            transform: 'scale(1.004)',
            cursor: 'pointer',
            boxShadow: '0px 1.2px 4px rgba(0,0,0,0.2)'
        });
        $(this).find('.judul-ebook').css('color', '#C00000');
    }).on('mouseout', function () {
        $(this).css({
            transform: 'scale(1)',
            cursor: 'auto',
            boxShadow: 'none'
        })
        $(this).find('.judul-ebook').css('color', 'black');
    });


})

$(document).ready(function () {
    $('.owl-carousel.video').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        dots: false,
        navText: ["<i class='fa fa-angle-left text-dark'></i>", "<i class='fa fa-angle-right text-dark'></i>"],
        nav: true,
        items: 1
    })

    $('.item-video').on('mouseover', function () {
        $(this).css({
            transform: 'scale(1.004)',
            cursor: 'pointer',
            boxShadow: '0px 1.2px 4px rgba(0,0,0,0.2)'
        });
        $(this).find('.deskripsi-video h3').css('color', '#C00000');
    }).on('mouseout', function () {
        $(this).css({
            transform: 'scale(1)',
            cursor: 'auto',
            boxShadow: 'none'
        })
        $(this).find('.deskripsi-video h3').css('color', 'black');
    });

})

$(document).ready(function () {

    $('.owl-carousel.shop').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        dots: false,
        navText: ["<i class='fa fa-angle-left text-dark'></i>", "<i class='fa fa-angle-right text-dark'></i>"],
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: true
            },
            1000: {
                items: 5,
                nav: true,
                loop: true
            }
        }
    })

    $('.item-produk').on('mouseover', function () {
        $(this).css({
            transform: 'scale(1.004)',
            cursor: 'pointer',
            boxShadow: '0px 1.2px 4px rgba(0,0,0,0.2)'
        });
        $(this).find('.judul-barang').css('color', '#C00000');
    }).on('mouseout', function () {
        $(this).css({
            transform: 'scale(1)',
            cursor: 'auto',
            boxShadow: 'none'
        })
        $(this).find('.judul-barang').css('color', 'black');
    });

    $('.i-tp').on('mouseover', function () {
        $(this).next().css({
            visibility: 'visible',
            opacity: '1',
            transition: '.5s',
            transitionDelay: '.3s'
        })
    }).on('mouseout', function () {
        $(this).next().css({
            transitionDelay: '.5s',
            transition: '.5s',
            visibility: 'hidden',
            opacity: '0',
        })
    })

    $('.i-sp').on('mouseover', function () {
        $(this).next().css({
            visibility: 'visible',
            opacity: '1',
            transition: '.5s',
            transitionDelay: '.3s'
        })
    }).on('mouseout', function () {
        $(this).next().css({
            transitionDelay: '.5s',
            transition: '.5s',
            visibility: 'hidden',
            opacity: '0'
        })
    })

    $('.i-bl').on('mouseover', function () {
        $(this).next().css({
            visibility: 'visible',
            opacity: '1',
            transition: '.5s',
            transitionDelay: '.3s'
        })
    }).on('mouseout', function () {
        $(this).next().css({
            transitionDelay: '2s',
            transition: '.5s',
            visibility: 'hidden',
            opacity: '0',
            transition: '.5s'
        })
    })
})