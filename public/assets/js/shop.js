$(document).ready(function () {

    $('.dropdown-item#editdata').click(function () {
        let getId = $(this).attr('my-idproduk');

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'shop/fetchdata/' + getId,
            success: function (data) {
                console.log(data)
                $('input[name="judul"]').val(data.judul);
                $('input[name="deskripsi"]').val(data.deskripsi);
                $('input[name="kategori"]').val(data.kategori);
                $('input[name="link_bl"]').val(data.link_bl);
                $('input[name="link_tp"]').val(data.link_tp);
                $('input[name="link_sp"]').val(data.link_sp);
                $('select[name="id_tipe_data"]').val(data.id_tipe_data);
                $('img#prethumbnail').attr('src', 'uploaded/pic/' + data.thumbnail);
                $('form.ubahdataproduk').attr('action', '/shop/ubahdata/' + getId);
            },
            error: function (data) {
                console.log("Request Gagal");
            }
        });
    })

    $('.dropdown-item#hapusdata').on('click', function () {
        let getId = $(this).attr('my-idproduk');
        $('form#hapusdata').attr('action', '/shop/hapusdata/' + getId);
    })

    $('.i-bl').on('mouseover', function () {
        $(this).next().css({
            visibility: 'visible',
            opacity: '1',
            transition: '.5s',
            transitionDelay: '.3s'
        })
    }).on('mouseout', function () {
        $(this).next().css({
            transitionDelay: '2s',
            transition: '.5s',
            visibility: 'hidden',
            opacity: '0',
            transition: '.5s'
        })
    })

    console.log($('i.icon-onlineshop').next());

    $('.i-tp').on('mouseover', function () {
        $(this).next().css({
            visibility: 'visible',
            opacity: '1',
            transition: '.5s',
            transitionDelay: '.3s'
        })
    }).on('mouseout', function () {
        $(this).next().css({
            transitionDelay: '.5s',
            transition: '.5s',
            visibility: 'hidden',
            opacity: '0',
        })
    })

    $('.i-sp').on('mouseover', function () {
        $(this).next().css({
            visibility: 'visible',
            opacity: '1',
            transition: '.5s',
            transitionDelay: '.3s'
        })
    }).on('mouseout', function () {
        $(this).next().css({
            transitionDelay: '.5s',
            transition: '.5s',
            visibility: 'hidden',
            opacity: '0'
        })
    })

})