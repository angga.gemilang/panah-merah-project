$(document).ready(function () {

    $('#tabs li a:not(:first)').addClass('inactive');
    $('.containe4r').hide();
    $('.containe4r:first').show();

    $('#tabs li a').click(function () {
        var t = $(this).attr('id');
        if ($(this).hasClass('inactive')) { //this is the start of our condition 
            $('#tabs li a').addClass('inactive');
            $(this).removeClass('inactive');

            $('.containe4r').hide();
            $('#' + t + 'C').fadeIn('slow');
        }
    });

});

$(document).ready(function () {

    $('.owl-carousel.ebook').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        dots: false,
        navText: ["<i class='fa fa-angle-left text-dark'></i>", "<i class='fa fa-angle-right text-dark'></i>"],
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: true
            },
            1000: {
                items: 5,
                nav: true,
                loop: true
            }
        }
    })

    $('.link-ebook').on('mouseover', function () {
        $(this).css({
            transform: 'scale(1.004)',
            cursor: 'pointer',
            boxShadow: '0px 1.2px 4px rgba(0,0,0,0.2)'
        });
        $(this).find('.judul-ebook').css('color', '#C00000');
    }).on('mouseout', function () {
        $(this).css({
            transform: 'scale(1)',
            cursor: 'auto',
            boxShadow: 'none'
        })
        $(this).find('.judul-ebook').css('color', 'black');
    });


})