$(document).ready(function () {

    $('.dropdown-item#editdata').click(function () {
        let getId = $(this).attr('my-idebook');

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'ebook/fetchdata/' + getId,
            success: function (data) {
                console.log(data)
                $('input[name="judul"]').val(data.judul);
                $('input[name="deskripsi"]').val(data.deskripsi);
                $('input[name="kategori"]').val(data.kategori);
                $('select[name="id_tipe_data"]').val(data.id_tipe_data);
                $('img#prethumbnail').attr('src','uploaded/pic/' + data.thumbnail );
                $('p#prefile').text(data.file);
                $('form.ubahdataebook').attr('action', '/ebook/ubahdata/' + getId);
            },
            error: function (data) {
                console.log("Request Gagal");
            }
        });
    })

    $('.dropdown-item#hapusdata').on('click', function(){
        let getId = $(this).attr('my-idebook');
        $('form#hapusdata').attr('action','/ebook/hapusdata/' + getId);
    })

})