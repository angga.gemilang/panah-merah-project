$(function () {
    $('nav .menu .list-menu .link').filter(function () {
        return this.href == location.href
    }).parent().addClass('active').siblings().removeClass('active')
})

$('.navbar-slide').css('margin-top','-410px').css('transition','.3s');

$(window).scroll(function () {

    if($(window).scrollTop() > 194)
    {
        $('.navbar-slide').css('margin-top','0px').css('transition','.3s');
    } else 
    {
        $('.navbar-slide').css('margin-top','-110px').css('transition','.3s');
    }
    
});

$(document).ready(function () {

    $('.btn-option').on('click', function () {

        if ($(this).next().hasClass("show")) {
            $(this).css('display', '');
            $(this).next().removeClass("show").hide();

        } else {
            $(this).css('display', 'block');
            $(this).next().addClass("show").show();
        }
    })

    $('#carisemua').keyup(function () {
        var query = $(this).val();
        if (query != '') {
            $.ajax({
                url: "/cari",
                method: "GET",
                data: {
                    query: query,
                },
                success: function (data) {
                    $('#suggestion-box').fadeIn();
                    $('#suggestion-box').html(data);
                }
            });
        }
    });

    $(document).on('click', function(){
        $('#suggestion-box').fadeOut();
    })

    $('.modal').on('shown.bs.modal', function(e){
        e.preventDefault();
        $('html').css('overflow', 'hidden');
    })
    
    $('.modal').on('hidden.bs.modal', function(e) {
        e.preventDefault();
        $('html').css('overflow', 'auto');
    })

});