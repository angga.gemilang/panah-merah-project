$(document).ready(function () {

    $('.dropdown-item#editdata').click(function () {
        let getId = $(this).attr('my-idvideo');

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'video/fetchdata/' + getId,
            success: function (data) {
                console.log(data)
                $('input[name="judul"]').val(data.judul);
                $('input[name="deskripsi"]').val(data.deskripsi);
                $('input[name="kategori"]').val(data.kategori);
                $('select[name="id_tipe_data"]').val(data.id_tipe_data);
                $('input[name="link"]').val(data.file);
                $('img#prethumbnail').attr('src','uploaded/pic/' + data.thumbnail);
                $('form.ubahdatavideo').attr('action', '/video/ubahdata/' + getId);
            },
            error: function (data) {
                console.log("Request Gagal");
            }
        });
    })

    $('.dropdown-item#hapusdata').on('click', function(){
        let getId = $(this).attr('my-idvideo');
        $('form#hapusdata').attr('action','/video/hapusdata/' + getId);
    })

})