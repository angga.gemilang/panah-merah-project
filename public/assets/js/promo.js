$(document).ready(function () {

    $('#tabs li a:not(:first)').addClass('inactive');
    $('.containe4r').hide();
    $('.containe4r:first').show();

    $('#tabs li a').click(function () {
        var t = $(this).attr('id');
        if ($(this).hasClass('inactive')) 
        {
            $('#tabs li a').addClass('inactive');
            $(this).removeClass('inactive');

            $('.containe4r').hide();
            $('#' + t + 'C').fadeIn('slow');
        }
    });

});