@extends('layouts.template')

@section('title')
    {{ $dvideo->judul }}
@endsection

@push('css')

<link rel="stylesheet" href="{{ asset('assets') }}/css/detailvideo.css">

@endpush

@section('content')

<div class="row">

    <div class="banner">
        <img src="{{ asset('assets') }}/pic/carousel1.jpg" alt="">
    </div>

</div>

<div class="container main-content">

    <div class="row mt-5 m-2">

        <div class="col-md-5 left">

            <h1 class="min-height: 100px;">{{ $dvideo->judul }}</h1>

            <p class="text-light mt-5"><i class="fas fa-dot-circle pr-2"
                    style="font-size: 13px;"></i>{{ $dvideo->kategori }}</p>

            <div class="input-group side-cari-wrapper mt-5 mb-4">

                <input type="text" class="form-control" placeholder="Cari">
                <i class="fa fa-search"></i>

            </div>

            <h4 class="mb-4">Rekomendasi Video Lainnya</h4>

            @foreach($related as $rel)
            <div class="content-related mb-4">
                <div class="col-12">
                    <div class="row">

                        <a href="{{ url('/video/detailvideo/' . $rel->id) }}" class="text-dark">
                            <div class="col-md-5" style="float: left;">

                                <div class="side-iframe">
                                    <img src="{{ asset('uploaded/pic/' . $rel->thumbnail) }}" alt="{{ $rel->judul }}">
                                    <i class="far fa-play-circle"></i>
                                </div>

                            </div>

                            <div class="col-md-6" style="float: left; margin-left: -24px !important; margin-top: 2px; margin-top: -5px;">
                                <p style="font-weight: 500; font-size: 18px;">
                                    {{ $rel->judul }}
                                </p>

                            </div>
                            <div class="clearfix"></div>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach

        </div>

        <div class="col-7 right">
            <div class="content-right">

                <button class="download">Bagikan Video</button>
                <button class="share"><i class="fas fa-share"></i></button>
                <br><br><br>
                <div class="iframe-wrapper main-iframe">
                    <iframe class="main-video" src="{{ $dvideo->file }}" frameborder="0" allowfullscreen></iframe>
                </div>
                <h4 class="mt-3 mb-3">Deskripsi</h4>
                <p>{{ $dvideo->deskripsi }}</p>

            </div>

        </div>
    </div>

</div>

@endsection

@push('js')
<script src="{{ asset('assets') }}/js/detailvideo.js"></script>
@endpush