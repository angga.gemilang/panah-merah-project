@extends('layouts.template')

@section('title','Beranda')

@push('css')
<link rel="stylesheet" href="{{ asset('assets') }}/css/dashboard.css">
@endpush

@section('content')

<div class="slide-container row-1 slider-upper">
    <div class="main-content"> </div>
    <button class="left-arrow"><i class="fas fa-chevron-left"></i></button>
    <button class="right-arrow"><i class="fas fa-chevron-right"></i></button>
    <div class="dot-container"> </div>
    <div class="captionText"></div>
</div>

<div class="row row-2 mt-5 ebook-tutorial p-relative">
    <div class="container">
        <h4 class="judul-content">E - book Tutorial</h4>
    </div>
</div>

<div class="row">
    <div class="container">

        <div class="owl-carousel ebook owl-theme">
            @foreach($slice as $sc)
            <a id="link-ebook" href="{{ url('ebook/detailebook/' . $sc->id . '/' . $sc->kategori) }}">
                <div class="item-ebook link-ebook ml-3">
                    <img src="{{ asset('uploaded/pic/' . $sc->thumbnail) }}" alt="">
                    <h4 class="judul-ebook">{{ $sc->judul }}</h4>
                </div>
                <form action="{{ url('/ebook/pdfpreview/' . $sc->id ) }}"  target="_blank">
                    <input type="submit" class="btn-preview btn btn-danger" value="Preview" />
                </form>
            </a>
            @endforeach
        </div>

    </div>
</div>

<div class="row row-2 mt-4 mb-3 ebook-tutorial">

    <div class="container">

        <h4 class="judul-content">Video Tutorial</h4>
    </div>

</div>

<div class="row mb-2">
    <div class="container">
        <div class="owl-carousel video owl-theme">
            @foreach($slicevid as $vid)
            <a href="{{ url('video/detailvideo/' . $vid->id . '/' . $vid->kategori) }}" id="link-video">
                <div class="item item-video">
                    <div class="thumbnail-video">
                        <img src="{{ asset('uploaded/pic/' . $vid->thumbnail ) }}" alt="{{ $vid->judul }}">
                    </div>
                    <div class="deskripsi-video">
                        <h3 class="text-capitalize">
                            {{ $vid->judul }}
                        </h3>
                        <p class="mt-3">
                            {{ $vid->deskripsi }}
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</div>

<div class="row row-2 mt-5 ebook-tutorial">
    <div class="container">
        <h4 class="judul-content">Shop</h4>
    </div>
</div>

<div class="row">
    <div class="container">
        <div class="owl-carousel shop owl-theme">

            @foreach($sliceprod as $prod)
            <a id="link-shop" href="{{ url('shop/detailproduk/' . $prod->id . '/' . $prod->kategori  ) }}">
                <div class="item item-produk">
                    <img src="{{ asset('uploaded/pic/' . $prod->thumbnail ) }}">
                    <p class="judul-barang text-capitalize pl-1 pr-1">
                        @if(strlen($prod->judul) > 18)
                        {{substr($prod->judul, 0,20) . ' ...' }}
                        @else
                        {{substr($prod->judul, 0,18)}}
                        @endif
                    </p>
                    <p style="mt-5 text-transform: capitalize; float: left;" class="pl-1">
                        Tersedia di
                    </p>
                    <span class="kateg pl-2 pr-1" style="float: left;">
                        <i class="icon-onlineshop fas fa-circle i-bl"></i>
                        <div class="toltip t-bl">Bukalapak</div>
                        <i class="icon-onlineshop fas fa-circle i-tp"></i>
                        <div class="toltip t-tp">Tokopedia</div>
                        <i class="icon-onlineshop fas fa-circle i-sp"></i>
                        <div class="toltip t-sp">Shopee</div>
                    </span>
                    <div class="clearfix"></div>
                </div>
            </a>
            @endforeach

        </div>
    </div>
</div>

@endsection

@push('js')
<script src="{{ asset('assets') }}/js/dashboard.js"></script>
@endpush