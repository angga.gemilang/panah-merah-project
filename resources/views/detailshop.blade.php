@extends('layouts.template')

@section('title')
{{ $dshop->judul }}
@endsection

@push('css')

<link rel="stylesheet" href="{{ asset('assets') }}/css/detailshop.css">

@endpush

@section('content')

<div class="row">

    <div class="banner">
        <img src="{{ asset('assets') }}/pic/carousel1.jpg" alt="">
        <p>{{ $dshop->judul }}</p>
    </div>

    <div class="container main-content">

        <div class="breadcrumb">
            <ul>
                <li><a href="{{ route('dashboard') }}">Home</a>
                <li><a href="{{ route('ebook') }}" class="cn">Shop</a></li>
                <li><a href="#" class="cn">{{ $dshop->judul }}</a></li>
            </ul>
        </div>

    </div>
</div>

<div class="container">

    <div class="row mt-5 m-2">

        <div class="col-5 thumbnail_layout mr-5">
            <div class="exzoom" id="exzoom">
                <div class="exzoom_img_box" style="background: #ffffff !important;">
                    <ul class='exzoom_img_ul'>
                        <li><img src="{{ asset('uploaded/pic/' . $dshop->thumbnail) }}"></li>
                    </ul>
                </div>
                <div class="exzoom_nav" style="margin-left: -22px; margin-top: 15px;"></div>
            </div>
        </div>

        <div class="col-md-5">
            <p class="title mt-1">{{ $dshop->judul }}</p>
            <p><i class="fas fa-dot-circle pr-2" style="font-size: 13px;"></i>{{ $dshop->kategori }}</p>
            <div class="text">
                @php
                $jumlahkarakter = strlen($dshop->deskripsi);
                @endphp
                @if($jumlahkarakter > 534)
                {{substr($dshop->deskripsi, 0, 535) }}
                @else
                {{ $dshop->deskripsi }}
                @endif
            </div>
            <div class="full-text">
                {{substr($dshop->deskripsi, 521,$jumlahkarakter) }}
            </div>

            @php
            $jumlahkarakter = strlen($dshop->deskripsi);
            @endphp
            @if($jumlahkarakter > 534)
            <i class="fas fa-chevron-right" style="font-size: 13px;"></i>
            <span class="view-btn" style="font-size: 15px; margin-left: 3px;">
                View More
            </span>
            @endif

            <div class="wrap" style="margin-top: 25%;">
                <div class="bottom-cont">
                    <p class="font-weight-bold">Dapatkan di</p>
                    <a class="btn bl" href="{{ $dshop->link_bl }}"
                        style="color: #C00000; border: 1px solid #C00000;">Bukalapak</a>
                    <a class="btn tp" href="{{ $dshop->link_tp }}"
                        style="color: #28A745; border: 1px solid #28A745;">Tokopedia</a>
                    <a class="btn sp" href="{{ $dshop->link_sp }}"
                        style="color: #EA501F; border: 1px solid #EA501F;">Shopee</a>
                </div>
            </div>
        </div>

    </div>

    <ul id="tabs">

        <li><a id="tab1">Deksripsi</a></li>
        <li><a id="tab2">Product Tags</a></li>

    </ul>
    <div class="containe4r" id="tab1C">
        <p>{{ $dshop->deskripsi }}</p>
    </div>
    <div class="containe4r" id="tab2C">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum vero nobis
        fuga,
        possimus at quisquam beatae, molestiae impedit laudantium cumque, voluptatum ipsum saepe labore nihil
        suscipit
        ex eaque laboriosam perspiciatis?Dolorem repellat cum quis aliquid dolor velit odit qui quo sunt esse.
        Repudiandae dolorum expedita optio vitae repellat, modi nisi voluptates eligendi error impedit debitis minus
        nostrum quibusdam, doloremque distinctio.
    </div>

    <p class="ml-2 mt-4 title-similar">You May Also Like</p>
    <hr class="hr-similar">

    <div class="row ml-0">
        <div class="owl-carousel shop owl-theme">

            @foreach($related as $rel)
            <a id="link-shop" href="{{ url('shop/detailreluk/' . $rel->id . '/' . $rel->kategori  ) }}">
                <div class="item item-produk">
                    <img src="{{ asset('uploaded/pic/' . $rel->thumbnail ) }}">
                    <p class="judul-barang text-capitalize pl-1 pr-1">
                        @if(strlen($rel->judul) > 18)
                        {{substr($rel->judul, 0,20) . ' ...' }}
                        @else
                        {{substr($rel->judul, 0,18)}}
                        @endif
                    </p>
                    <p style="mt-5 text-transform: capitalize; float: left;" class="pl-1">
                        Tersedia di
                    </p>
                    <span class="kateg pl-2 pr-1" style="float: left;">
                        <i class="icon-onlineshop fas fa-circle i-bl"></i>
                        <div class="toltip t-bl">Bukalapak</div>
                        <i class="icon-onlineshop fas fa-circle i-tp"></i>
                        <div class="toltip t-tp">Tokopedia</div>
                        <i class="icon-onlineshop fas fa-circle i-sp"></i>
                        <div class="toltip t-sp">Shopee</div>
                    </span>
                    <div class="clearfix"></div>
                </div>
            </a>
            @endforeach

        </div>
    </div>
</div>

@endsection

@push('js')
<script src="{{ asset('assets') }}/js/detailshop.js"></script>
@endpush