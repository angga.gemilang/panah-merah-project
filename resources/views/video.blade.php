@extends('layouts.template')

@section('title','Panduan Video')

@push('css')

<link rel="stylesheet" href="{{ asset('assets') }}/css/video.css">

@endpush

@section('content')

<div class="row">

    <div class="banner">
        <img src="{{ asset('assets') }}/pic/carousel1.jpg" alt="">
        <p>All Collection Video</p>
        <button type="button" class="btn btn-danger rounded-circle btn-tambah-data" data-toggle="modal"
            data-target="#tambahdata">
            <i class="fa fa-plus"></i>
        </button>

    </div>

    <div class="container main-content">

        <div class="breadcrumb">
            <ul>
                <li><a href="{{ route('dashboard') }}">Home</a>
                <li><a href="{{ route('video') }}" class="cn">Video</a></li>
            </ul>
        </div>

    </div>
</div>

<div class="container">

    <div class="row m-1">

        <div class="col-3 sidebar mr-5 mt-2">

            <form action="{{url('/video/result')}}" method="get">
                <div class="form-group cari-wrapper mb-4">
                    <input type="text" class="w-100 form-control" name="search_query" placeholder="Cari Video">
                    <i class="fa fa-search"></i>
                </div>
            </form>


            <h3>Categories</h3>
            <hr>

            <div class="kategori-list mt-5">
                <ul>
                    <li><a class="active" href="#">Seluruh Kategori</a>
                    <li><a href="#">Budidaya Bunga</a></li>
                    <li><a href="#">Budidaya Sayur</a></li>
                    <li><a href="#">Budidaya Buah</a></li>
                    <li><a href="#">Hama Dan Penyakit</a></li>
                </ul>

            </div>
        </div>

        <div class="col-8">

            <h4 class="judul-content mb-5">Video Tutorial</h4>

            <div class="row">

                @foreach($video as $vd)
                <div class="col-xs-12 col-xl-5 card-call">
                    <div class="btn-group" style="position: absolute; right: 0; bottom: 16px;">
                        <button type="button" class="btn btn-option">
                            <i class="fa fa-ellipsis-v" style="font-size: 20px;"></i>
                        </button>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item" my-idvideo="{{ $vd->id }}" id="editdata"
                                data-toggle="modal" data-target="#editmodal">
                                Edit
                            </a>
                            <button class="dropdown-item" id="hapusdata" my-idvideo="{{ $vd->id }}" data-toggle="modal"
                                data-target="#hapusmodal">
                                Hapus
                            </button>
                        </div>
                    </div>
                    <a class="link-tulisan" href="{{ url('/video/detailvideo/' . $vd->id . '/' . $vd->kategori  ) }}"
                        style="color: #000000; text-decoration: none;">
                        <div class="thumbnail">
                            <img src="{{ asset('uploaded/pic/' . $vd->thumbnail) }}" alt="{{ $vd->judul }}">
                            <i class="far fa-play-circle"></i>
                        </div>
                        <div class="deskripsi p-3" style="margin-top: -13px;">
                            <p class="kategori mb-1 mt-1">
                                <i class="fas fa-dot-circle"></i>
                                {{ $vd->kategori }}
                            </p>
                            <p class="judul-barang mb-1">{{ $vd->judul }}</p>
                            <p class="publish">Published: {{ $vd->created_at }}</p>
                        </div>
                    </a>
                </div>
                @endforeach

            </div>
        </div>
    </div>

    <div class="modal fade" id="hapusmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Perhatian</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Apakah Anda Yakin Akan Menghapus Data Ini?</p>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali
                    </button>
                    <form action="" id="hapusdata" method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">Hapus
                            Data
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" style="z-index: 9999;" id="tambahdata" tabindex="-1" role="dialog"
        aria-labelledby="tambahdatalabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tambahdatalabel">Tambah Data Video</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('form-input.tambahvideo')
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" style="z-index: 9999;" id="editmodal" tabindex="-1" role="dialog"
        aria-labelledby="editdatalabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editdatalabel">Edit Data Video</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('form-input.editvideo')
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('js')
<script src="{{ asset('assets') }}/js/video.js"></script>
@endpush
