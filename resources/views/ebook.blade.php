@extends('layouts.template')

@section('title','Daftar Ebook')

@push('css')

<link rel="stylesheet" href="{{ asset('assets') }}/css/ebook.css">

@endpush

@section('content')

<div class="row">

    <div class="banner">
        <img src="{{ asset('assets') }}/pic/carousel1.jpg" alt="">
        <p>All Collection Ebooks</p>
        <button type="button" class="btn btn-danger rounded-circle btn-tambah-data" style="z-index: 1;"
            id="btnTambahData" data-toggle="modal" data-target="#tambahmodal">
            <i class="fa fa-plus"></i>
        </button>

    </div>

    <div class="container main-content">

        <div class="breadcrumb">
            <ul>
                <li><a href="{{ route('dashboard') }}">Home</a>
                <li><a href="{{ route('ebook') }}" class="cn">Ebook</a></li>
            </ul>
        </div>

    </div>
</div>

<div class="container c-2">

    <div class="row m-3">

        <div class="col-md-3 sidebar mr-5 mt-2">

            <form action="{{url('/ebook/result')}}" method="get">
                <div class="form-group cari-wrapper mb-4">
                    <input type="text" class="w-100 form-control" name="search_query" placeholder="Cari Ebook">
                    <i class="fa fa-search"></i>
                </div>
            </form>

            <h3>Categories</h3>

            <hr>

            <div class="kategori-list mt-5">
                <ul>
                    <li><a class="active" href="#">Seluruh Kategori</a>
                    <li><a href="#">Budidaya Bunga</a></li>
                    <li><a href="#">Budidaya Sayur</a></li>
                    <li><a href="#">Budidaya Buah</a></li>
                    <li><a href="#">Hama Dan Penyakit</a></li>
                </ul>

            </div>

        </div>

        <div class="col-8 ml-2">

            <h4 class="judul-content">Ebook Tutorial</h4>

            <div class="row">

                @foreach($ebook as $eb)
                <div class="col-xs-12 col-xl-3 card-call">
                    <div class="btn-group" style="position: absolute; right: 0;">
                        <button type="button" class="btn btn-option">
                            <i class="fa fa-ellipsis-v" style="font-size: 20px;"></i>
                        </button>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item" id="editdata" data-toggle="modal" data-target="#editmodal"
                                my-idebook="{{ $eb->id }}">
                                Edit
                            </a>
                            <button class="dropdown-item" my-idebook="{{ $eb->id }}" id="hapusdata" data-toggle="modal"
                                data-target="#hapusmodal">
                                Hapus
                            </button>
                        </div>
                    </div>
                    <a class="link-tulisan" href="{{ url('/ebook/detailebook/' . $eb->id . '/' . $eb->kategori) }}"
                        style="color: #000000; text-decoration: none;">
                        <img src="{{ asset('uploaded/pic/' . $eb->thumbnail) }}">
                        <p class="judul-barang">
                            {{ $eb->judul }}
                        </p>
                    </a>
                    <a href="{{ url('/ebook/pdfpreview/' . $eb->id ) }}" target="_blank">
                        <button class="btn btn-danger btn-download" style="z-index: 2;">Preview</button>
                    </a>
                </div>
                @endforeach

            </div>
        </div>

    </div>

    <div class="modal fade" id="hapusmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Perhatian</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Apakah Anda Yakin Akan Menghapus Data Ini?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Kembali
                    </button>
                    <form action="" id="hapusdata" method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">Hapus
                            Data
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="tambahmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tambahdatalabel">Tambah Data Ebook</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('form-input.tambahebook')
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" style="z-index: 9999;" id="editmodal" tabindex="-1" role="dialog"
        aria-labelledby="editdatalabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editdatalabel">Edit Data Ebook</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('form-input.editebook')
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('js')
<script src="{{ asset('assets') }}/js/ebook.js"></script>
@endpush
