<form method="post" action="" class="ubahdataebook" enctype="multipart/form-data">

    {{ csrf_field() }}

    <div class="form-group">
        <label>Judul</label>
        <input type="text" aria-label="Masukkan Judul" class="form-control" name="judul" placeholder="Masukkan Judul">
    </div>

    <div class="form-group">
        <label>Deskripsi</label>
        <input type="text" aria-label="Masukkan Deskripsi" class="form-control" name="deskripsi"
            placeholder="Masukkan Deskripsi">
    </div>

    <div class="form-group">
        <label>Kategori</label>
        <input type="text" aria-label="Kategori" class="form-control" name="kategori" placeholder="Masukkan Kategori">
    </div>

    <div class="form-group">
        <label>Jenis File</label>
        <select name="id_tipe_data" class="form-control">
            <option>Pilih Tipe Data</option>
            @foreach($tipe as $tp)
            <option value="{{ $tp->id }}">{{ $tp->tipe }}</option>
            @endforeach
        </select>
    </div>

    <label class="mb-3">Ubah Thumbnail</label><br>
    <img src="" id="prethumbnail" alt="" style="width: 90px; height: 90px; margin-bottom: 6px;">
    <input type="file" name="thumbnail" class="mb-5 mt-2 ml-2"><br>
    
    <label>Ubah File</label><br>
    <p id="prefile" style="font-weight: 700;"></p>
    <input type="file" name="file" class="mb-4"><br>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" name="submit" class="btn btn-primary btn-tambah">
    </div>
</form>
