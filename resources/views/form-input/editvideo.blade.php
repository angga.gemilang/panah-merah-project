<form method="post" action="" class="ubahdatavideo" enctype="multipart/form-data">

    {{ csrf_field() }}

    <div class="form-group">
        <label>Judul</label>
        <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul">
    </div>

    <div class="form-group">
        <label>Deskripsi</label>
        <input type="text" class="form-control" name="deskripsi"
            placeholder="Masukkan Deskripsi">
    </div>

    <div class="form-group">
        <label>Kategori</label>
        <input type="text" class="form-control" name="kategori" placeholder="Masukkan Kategori">
    </div>

    <div class="form-group">
        <label>Jenis File</label>
        <select name="id_tipe_data" class="form-control">
            <option>Pilih Tipe Data</option>
            @foreach($tipe as $tp)
            <option value="{{ $tp->id }}">{{ $tp->tipe }}</option>
            @endforeach
        </select>
    </div>


    <label class="mb-3">Ubah Thumbnail</label><br>
    <img src="" id="prethumbnail" alt="" style="width: 200px; margin-bottom: 6px;">
    <input type="file" name="thumbnail" class="mb-3 mt-3"><br>

    <label>Masukkan Link Video</label><br>
    <input type="text" class="form-control" name="link" placeholder="Masukkan Link Video"><br>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" name="submit" class="btn btn-primary btn-tambah">
    </div>
</form>
