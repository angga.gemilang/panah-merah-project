<form method="post" action="{{ route('tambahshop') }}" enctype="multipart/form-data">

    {{ csrf_field() }}

    <label>Import Data</label><br>
    <input type="file" name="filedata" class="mb-4 mt-1"><br>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" name="submit" class="btn btn-primary btn-tambah">
    </div>
</form>
