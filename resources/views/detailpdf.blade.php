<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $pdf->judul }}</title>
    <style>
        body,
        html {
            overflow: hidden;
            margin: 0;
            padding: 0;
        }
        object
        {
            width: 100%;
            height: 100vh;
        }

    </style>
</head>

<body >
    <object data="{{ url('uploaded/file/' . $pdf->file) }}#toolbar=1&amp;navpanes=1&amp;scrollbar=1&amp;zoom=60" type="application/pdf">
        <param name="view" values="Fit" />
    </object>
</body>

</html>
