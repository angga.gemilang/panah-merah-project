@extends('layouts.template')

@section('title')
{{ $debook->judul }}
@endsection

@push('css')

<link rel="stylesheet" href="{{ asset('assets') }}/css/detailebook.css">

@endpush

@section('content')

<div class="row">

    <div class="banner">
        <img src="{{ asset('assets') }}/pic/carousel1.jpg" alt="">
        <p>{{ $debook->judul }}</p>
    </div>

    <div class="container main-content">

        <div class="breadcrumb">
            <ul>
                <li><a href="{{ route('dashboard') }}">Home</a>
                <li><a href="{{ route('ebook') }}" class="cn">Ebook</a></li>
                <li><a href="{{ url('/ebook/detailebook/' . $debook->id . '/' . $debook->kategori) }}"
                        class="cn">{{ $debook->judul }}</a></li>
            </ul>
        </div>

    </div>
</div>

<div class="container">

    <div class="row mt-5 m-2">

        <div class="col-5 thumbnail_layout mr-5">
            <img src="{{ asset('uploaded/pic/' . $debook->thumbnail) }}">
        </div>

        <div class="col-5" style="position: relative;">
            <p class="title mt-1">{{ $debook->judul }}</p>
            <p><i class="fas fa-dot-circle pr-2" style="font-size: 13px;"></i>{{ $debook->kategori }}</p>
            <p class="mt-4">{{ $debook->deskripsi }}</p><br>

            <div class="bottom-cont">
                <a href="" class="btn btn-danger" id="btndownload" download="{{ $debook->file }}"
                    style="color: #ffffff; font-size: 14pt; width: 280px;">
                    Download
                </a>
                <a href="{{ url('/ebook/pdfpreview/' . $debook->id ) }}" target="_blank" id="btnpreview" class="btn">
                    Preview
                </a>
                <p><i class="fas fa-download pr-2 mt-3"></i>200 Downloaded</p>
            </div>
        </div>
    </div>

    <ul id="tabs">
        <li><a id="tab1">Deksripsi</a></li>
        <li><a id="tab2">Product Tags</a></li>
    </ul>
    <div class="containe4r" id="tab1C">
        {{ $debook->deskripsi }}
    </div>
    <div class="containe4r" id="tab2C">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum vero nobis fuga,
        possimus at quisquam beatae, molestiae impedit laudantium cumque, voluptatum ipsum saepe labore nihil suscipit
        ex eaque laboriosam perspiciatis?Dolorem repellat cum quis aliquid dolor velit odit qui quo sunt esse.
        Repudiandae dolorum expedita optio vitae repellat, modi nisi voluptates eligendi error impedit debitis minus
        nostrum quibusdam, doloremque distinctio.</div>

    <p class="ml-2 mt-4 title-similar">You May Also Like</p>
    <hr class="hr-similar">

    <div class="row">

        <div class="owl-carousel ebook owl-theme">
            @foreach($related as $rel)
            <a id="link-ebook" href="{{ url('ebook/detailebook/' . $rel->id . '/' . $rel->kategori) }}">
                <div class="item-ebook link-ebook ml-3">
                    <img src="{{ asset('uploaded/pic/' . $rel->thumbnail) }}" alt="">
                    <h4 class="judul-ebook">{{ $rel->judul }}</h4>
                </div>
                <form action="{{ url('/ebook/pdfpreview/' . $rel->id ) }}" target="_blank">
                    <input type="submit" class="btn-preview btn btn-danger" value="Preview" />
                </form>
            </a>
            @endforeach
        </div>

    </div>
</div>
@endsection

@push('js')
<script src="{{ asset('assets') }}/js/detailebook.js"></script>
@endpush