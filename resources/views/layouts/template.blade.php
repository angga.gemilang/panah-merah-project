<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Link style -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <!-- Link XZoom -->
    <link rel="stylesheet" href="{{ asset('assets/Product-Carousel-Magnifying-Effect-exzoom/src/jquery.exzoom.css') }}">

    @stack('css')

    <!-- Link Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets') }}/fontawesome-free-5.11.2-web/css/all.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- link toastr -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/toastr.css">

    {{-- link owl --}}
    <link rel="stylesheet" href="{{ asset('assets') }}/OwlCarousel2-2.3.4/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.css">

    <title>@yield('title') - Ayo Menanam</title>

</head>

<body>

    @include('partials.navbar')
    @yield('content')
    @include('partials.footer')

    <script rel="stylesheet" href="{{ asset('assets') }}/fontawesome-free-5.11.2-web/css/all.js"></script>
    <script src="{{ asset('assets/js/jquery-3.4.1.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="{{ asset('assets/js/toastr.min.js') }}"></script>
    <script>
        @if(Session::has('success'))
        toastr.success('{!! session('
            success ') !!}', 'Success', {
                "showMethod": "slideDown",
                "hideMethod": "slideUp",
                timeOut: 4000
            });
        @endif
    </script>
    <script src="{{ asset('assets') }}/OwlCarousel2-2.3.4/dist/owl.carousel.js"></script>
    <script src="{{ asset('assets') }}/Product-Carousel-Magnifying-Effect-exzoom/src/jquery.exzoom.js"></script>
    <script src="{{ asset('assets') }}/js/script.js"></script>
    @stack('js')
</body>

</html>