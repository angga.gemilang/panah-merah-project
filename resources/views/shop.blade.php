@extends('layouts.template')

@section('title','Lihat Semua Produk')

@push('css')

<link rel="stylesheet" href="{{ asset('assets') }}/css/shop.css">

@endpush

@section('content')

<div class="row">

    <div class="banner">
        <img src="{{ asset('assets') }}/pic/carousel1.jpg" alt="">
        <p>All Collection Products</p>
        <button type="button" class="btn btn-danger rounded-circle btn-tambah-data" data-toggle="modal"
            data-target="#tambahdata">
            <i class="fa fa-plus"></i>
        </button>

    </div>

    <div class="container main-content">

        <div class="breadcrumb">
            <ul>
                <li><a href="{{ route('dashboard') }}">Home</a>
                <li><a href="{{ route('shop') }}" class="cn">Products</a></li>
            </ul>
        </div>

    </div>
</div>

<div class="container">

    <div class="row m-3">

        <div class="col-md-3 sidebar mr-5 mt-2">

            <form action="{{url('/shop/result')}}" method="get">
                <div class="form-group cari-wrapper mb-4">
                    <input type="text" class="w-100 form-control" name="search_query" placeholder="Cari Produk">
                    <i class="fa fa-search"></i>
                </div>
            </form>

            <h3>Categories</h3>
            <hr>

            <div class="kategori-list mt-5">
                <ul>
                    <li><a class="active" href="#">Seluruh Kategori</a>
                    <li><a href="#">Budidaya Bunga</a></li>
                    <li><a href="#">Budidaya Sayur</a></li>
                    <li><a href="#">Budidaya Buah</a></li>
                    <li><a href="#">Hama Dan Penyakit</a></li>
                </ul>

            </div>
        </div>

        <div class="col-8 ml-2">

            <h4 class="judul-content">Our Products</h4>

            <div class="row">

                @foreach($produk as $pr)
                <div class="col-xs-12 col-xl-3 card-call">
                    <div class="btn-group" style="position: absolute; right: 0;">
                        <button type="button" class="btn btn-option">
                            <i class="fa fa-ellipsis-v" style="font-size: 20px;"></i>
                        </button>
                        <div class="dropdown-menu">
                            <a href="#" id="editdata" class="dropdown-item" my-idproduk="{{ $pr->id }}"
                                data-toggle="modal" data-target="#editmodal">
                                Edit
                            </a>
                            <button class="dropdown-item" id="hapusdata" my-idproduk="{{ $pr->id }}" data-toggle="modal"
                                data-target="#hapusmodal">
                                Hapus
                            </button>
                        </div>
                    </div>
                    <a class="link-tulisan" href="{{ url('shop/detailproduk/' . $pr->id . '/' . $pr->kategori  ) }}"
                        style="color: #000000; text-decoration: none;">
                        <img src="{{ asset('uploaded/pic/' . $pr->thumbnail ) }}">
                        <p class="judul-barang text-capitalize">
                            @if(strlen($pr->judul) > 18)
                            {{substr($pr->judul, 0,20) . ' ...' }}
                            @else
                            {{substr($pr->judul, 0,18)}}
                            @endif
                        </p>
                        <p style="mt-5 text-transform: capitalize; float: left;">
                            Tersedia di
                        </p>
                        <span class="kateg pl-1" style="float: left;">
                            <i class="icon-onlineshop fas fa-circle i-bl"></i>
                            <div class="toltip t-bl">Bukalapak</div>
                            <i class="icon-onlineshop fas fa-circle i-tp"></i>
                            <div class="toltip t-tp">Tokopedia</div>
                            <i class="icon-onlineshop fas fa-circle i-sp"></i>
                            <div class="toltip t-sp">Shopee</div>
                        </span>
                        <div class="clearfix"></div>
                    </a>
                </div>
                @endforeach

            </div>
        </div>

    </div>

    <div class="modal fade" id="hapusmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Perhatian</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Apakah Anda Yakin Akan Menghapus Data Ini?</p>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali
                    </button>
                    <form id="hapusdata" action="" method="POST">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">Hapus
                            Data
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" style="z-index: 9999;" id="tambahdata" tabindex="-1" role="dialog"
        aria-labelledby="tambahdatalabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tambahdatalabel">Tambah Produk Shop</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('form-input.tambahshop')
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" style="z-index: 9999;" id="editmodal" tabindex="-1" role="dialog"
        aria-labelledby="editdatalabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editdatalabel">Edit Produk Shop</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('form-input.editshop')
                </div>
            </div>
        </div>
    </div>

</div>
</div>
@endsection

@push('js')
<script src="{{ asset('assets') }}/js/shop.js"></script>
@endpush