@extends('layouts.template')

@section('title','Promo')

@push('css')
<link rel="stylesheet" href="{{ asset('assets') }}/css/promo.css">
@endpush

@section('content')

<div class="container">

    <div class="row top-section">

        <div class="left float-left">
                <img src="{{ asset('assets') }}/pic/panahmerahlogo.png" alt="" style="width: 350px;">
        </div>

        <div class="right float-left">
            <h3>Promo Bibit Cap Panah Merah</h3>
        </div>

    </div>

    <div class="clearfix"></div>

    <div class="row">

        <ul id="tabs">
            <li><a id="tab1">Sedang Berjalan</a></li>
            <li><a id="tab2">Yang Akan Datang</a></li>
            <li><a id="tab3">Sudah Berakhir</a></li>
        </ul>
        <div class="containe4r" id="tab1C">
            @php
                $kosong = 0;
                if($kosong === 0)
                {
                    echo ("<p style='text-align: center; line-height: 370px;'>Tidak Ada Data</p>");
                }
            @endphp
        </div>
        <div class="containe4r" id="tab2C">
            @php
                $kosong = 0;
                if($kosong === 0)
                {
                    echo ("<p style='text-align: center; line-height: 370px;'>Tidak Ada Data</p>");
                }
            @endphp
        </div>
        <div class="containe4r" id="tab3C">
            @php
                $kosong = 0;
                if($kosong === 0)
                {
                    echo ("<p style='text-align: center; line-height: 370px;'>Tidak Ada Data</p>");
                }
            @endphp
        </div>
    </div>
</div>

@endsection

@push('js')
<script src="{{ asset('assets') }}/js/promo.js"></script>
@endpush