<div class="container-fluid footer">
    <div class="row justify-content-center"
        style="padding-left: 100px; padding-bottom: 80px; padding-top: 80px; margin-left: 10px;">
        <div class="identity">
            <img src="{{ asset('assets') }}/pic/am_logo.png" alt="Ayo Menanam" class="ml-0 mb-3">
            <p style="font-size: 25px;">Menanam Jadi Mudah</p>
        </div>
        <div class="explore part-1">
            <h5 class="mb-3"><b>Main Menu</b></h5>
            <hr width="108">
            <ul>
                <li><a href="{{ route('dashboard') }}">Home</a></li>
                <li><a href="{{ route('ebook') }}">Ebooks</a></li>
                <li><a href="{{ route('video') }}">Tutorial Video</a></li>
                <li><a href="{{ route('shop') }}">Shop</a></li>
                <li><a href="{{ route('promo') }}">Promo</a></li>
            </ul>
        </div>
        <div class="explore part-2">
            <h5 class="mb-3"><b>Official Store</b></h5>
            <hr width="90">
            <ul>
                <li><a href="#">Bukalapak</a></li>
                <li><a href="#">Tokopedia</a></li>
                <li><a href="#">Shopee</a></li>
            </ul>
        </div>
        <div class="explore part-3">
            <h5 class="mb-3"><b>More Updates</b></h5>
            <hr width="123">
            <a href="#">
                <i class="fab fa-facebook-f"></i>
            </a>
            <a href="#">
                <i class="fab fa-instagram"></i>
            </a>
            <a href="#">
                <i class="fab fa-twitter"></i>
            </a>
            <a href="#">
                <i class="fab fa-youtube"></i>
            </a>
        </div>
    </div>

    <div class="row justify-content-center" style=" box-shadow: 5px 2px 10px rgba(0, 0, 0, 0.3);">
        <h6 class="text-center pt-2 pb-1" style="text-align: center; font-size: 14px;">Copyright &copy; 2019 angga
            gemilang</h6>
    </div>