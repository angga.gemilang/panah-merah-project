<nav>
    <div class="container">
        <div class="row row-1 mt-2 mb-4">
            <div class="col sosmed-item" style="margin:0; padding: 0;">
                <a href="#"><i class="fab fa-facebook-f"></i></a>
                <a href="#"><i class="fab fa-twitter"></i></a>
                <a href="#"><i class="fab fa-pinterest"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
                <a href="#"><i class="fab fa-youtube"></i></a>
                <a href="#"><i class="fab fa-vimeo"></i></a>
            </div>
        </div>

        <div class="row row-2 mb-4">
            <div class="col-2 __left float-left" style="margin:0; padding: 0;">
                <img src="{{ asset('assets') }}/pic/am_logo.png" alt="Ayo Menanam">
            </div>
            <div class="col-5 __right" style="margin:0; padding: 0; right: 0; position: absolute;">
                <form action="" method="get">
                    {{ csrf_field() }}
                    <div class="form-group cari-wrapper mb-4">
                        <input type="text" class="w-100 form-control" name="query" id="carisemua" autocomplete="off"
                            placeholder="Masukkan Apa Yang Kamu Cari?">
                        <i class="fa fa-search"
                            style="position: absolute; right: 15px !important; top: 12px !important;"></i>
                        <div id="suggestion-box"></div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row row-3">
            <div class="col-3 __left float-left m-1 text-center">
                <p>Menanam Jadi Mudah</p>
            </div>
            <div class="col-8 __right float-right">
                <ul class="menu">
                    <li class="list-menu active">
                        <a href="{{ route('dashboard') }}" class="link">
                            Home
                        </a>
                    </li>
                    <li class="list-menu">
                        <a href="{{ route('ebook') }}" class="link">
                            Ebooks <i class="fa fa-chevron-down ml-1"></i>
                        </a>
                        <div class="dropdown d-ebook">
                            <ul>
                                <li><a href="#" class="buah">Buah</a></li>
                                <li><a href="#">Sayuran</a></li>
                                <li><a href="#">Bunga</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="list-menu">
                        <a href="{{ route('video') }}" class="link">
                            Tutorial Video <i class="fa fa-chevron-down ml-1"></i>
                        </a>
                        <div class="dropdown d-video">
                            <ul>
                                <li><a href="#" class="buah">Buah</a></li>
                                <li><a href="#">Sayuran</a></li>
                                <li><a href="#">Bunga</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="list-menu">
                        <a href="{{ route('shop') }}" class="link">
                            Shop <i class="fa fa-chevron-down ml-1"></i>
                        </a>
                        <div class="dropdown d-shop">
                            <ul>
                                <li><a href="#" class="buah">Buah</a></li>
                                <li><a href="#">Sayuran</a></li>
                                <li><a href="#">Bunga</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="list-menu">
                        <a href="{{ route('promo') }}" class="link">
                            Promo
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row row-3 navbar-slide" style="margin-top: -120px;">
            <div class="col-3 __left float-left m-1 text-center">
                <p>Menanam Jadi Mudah</p>
            </div>

            <div class="col-8 __right float-right">
                <ul class="menu">
                    <li class="list-menu active">
                        <a href="{{ route('dashboard') }}" class="link">
                            Home
                        </a>
                    </li>
                    <li class="list-menu">
                        <a href="{{ route('ebook') }}" class="link">
                            Ebooks <i class="fa fa-chevron-down ml-1"></i>
                        </a>
                        <div class="dropdown d-ebook">
                            <ul>
                                <li><a href="#" class="buah">Buah</a></li>
                                <li><a href="#">Sayuran</a></li>
                                <li><a href="#">Bunga</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="list-menu">
                        <a href="{{ route('video') }}" class="link">
                            Tutorial Video <i class="fa fa-chevron-down ml-1"></i>
                        </a>
                        <div class="dropdown d-video">
                            <ul>
                                <li><a href="#" class="buah">Buah</a></li>
                                <li><a href="#">Sayuran</a></li>
                                <li><a href="#">Bunga</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="list-menu">
                        <a href="{{ route('shop') }}" class="link">
                            Shop <i class="fa fa-chevron-down ml-1"></i>
                        </a>
                        <div class="dropdown d-shop">
                            <ul>
                                <li><a href="#" class="buah">Buah</a></li>
                                <li><a href="#">Sayuran</a></li>
                                <li><a href="#">Bunga</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="list-menu">
                        <a href="{{ route('promo') }}" class="link">
                            Promo
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>